import { Controller, Get, Post, Patch, Delete, Body, Param } from '@nestjs/common';
import { PrismaService } from './prisma.service';

@Controller('news')
export class NewsController {
  constructor(private prisma: PrismaService) {}

  @Get()
  async getAllNews() {
    return this.prisma.news();
  }

  @Post()
  async createNews(@Body() data) {
    return this.prisma.createNews(data);
  }

  @Patch(':id')
  async updateNews(@Param('id') id, @Body() data) {
    return this.prisma.updateNews(+id, data);
  }

  @Delete(':id')
  async deleteNews(@Param('id') id) {
    return this.prisma.deleteNews(+id);
  }
}
