import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

@Injectable()
export class PrismaService {
    prisma: PrismaClient;
    constructor() {
        this.prisma = new PrismaClient();
    }

    async news() {
        return this.prisma.news.findMany();
    }
    async createNews(data) {
        return this.prisma.news.create({ data });
    }
    async updateNews(id, data) {
        return this.prisma.news.update({ where: { id }, data });
    }
    async deleteNews(id) {
        return this.prisma.news.delete({ where: { id } });
    }
}



